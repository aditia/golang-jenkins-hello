FROM golang:1.15.2-alpine AS builder
LABEL stage=builder

RUN mkdir /app
ADD . /app

WORKDIR /app
# RUN go build -o main .
COPY ./main /app/./main

FROM alpine
WORKDIR /app
COPY --from=builder /app/ /app/

CMD ["/app/main"]